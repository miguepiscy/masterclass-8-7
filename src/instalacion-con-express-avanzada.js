const express = require('express');
const graphqlServer = require('graphql-server-express');
const bodyParser = require('body-parser');
const _ = require('lodash');
const APIError = require('../helpers/APIError');
const httpStatus = require('http-status');

const schema = require('../schemas');
const config = require('../../config/env');

const router = express.Router();

const graphqlExpress = graphqlServer.graphqlExpress;
const graphiqlExpress = graphqlServer.graphiqlExpress;

const libjwt = require('../lib/jwt');
const libCache = require('../lib/Cache');

router.use(
  '/graphql',
  bodyParser.json(),
  middlewareSeachCache,
  libjwt.addUser,
  graphqlExpress((request) => { // eslint-disable-line arrow-body-style
    console.log('user', request.user);
    return {
      schema,
      context: {
        user: request.user,
        requireLogin: () => {
          if (!_.has(request, 'user._id')) {
            throw new APIError(APIError.UNAUTHORIZED, httpStatus.UNAUTHORIZED);
          }
        },
        requireAdmin: () => {
          if (!_.has(request, 'user._id') || !_.get(request, 'user.isAdmin')) {
            throw new APIError(APIError.UNAUTHORIZEDADMIN, httpStatus.UNAUTHORIZED);
          }
        },
      },
      formatError: (err) => {
        console.error('status', err);
        return {
          message: err.message,
          status: err.originalError.status || 500,
          path: err.path,
        };
      },
    };
  }),
);

if (config.env === 'development') {
  router.use('/graphiql', graphiqlExpress({
    endpointURL: '/graphql',
  }));
}

module.exports = router;
