// npm install graphql-yoga --save
import { GraphQLServer, GraphQLScalarType } from 'graphql-yoga';
import { getPost, addPost } from './db/posts';
import { getUser, addUser } from './db/users';
import { getComment } from './db/comments';
import jwt from 'jsonwebtoken';
//   scalar Coordinates
// scalar Date

const typeDefs = `
  type Usuario {
    id: ID!,
    nombre: String!
  }

  type Post {
    id: ID!,
    titulo: String,
    autor: Usuario!,
    comentarios: [Comment],
  }

  type Comment {
    id: ID!,
    texto: String!,
  }
 type Query {
    post(id: ID!): Post
    user(id: ID!): Usuario
    me: Usuario
 }

 input PostInput {
   titulo: String
 }

 type Mutation {
    createUser(nombre: String): String!
    createPost(post: PostInput): String!
 }
`;

const resolvers = {
  Query: {
    post(_, { id }) {
      return getPost(id);
    },
    user(_, { id }) {
      return getUser(id)
        .then((user) => {
          console.log(user.id);
          return user;
        })      
    },
    me(_, args, ctx) {
      ctx.requireAuth();
      return ctx.userLoged;
    }
  },
  Post: {
    autor(parent) {
      return getUser(parent.userId);
    },
    comentarios(parent) {
      return parent.comentarios.map((comentarioId) => {
        return getComment(comentarioId);
      });
    }
  },
  Mutation: {
    createUser(_, user) {
      const userCreated = addUser(user);
      return jwt.sign(userCreated, 'secret');
    },
    createPost(_, post, ctx) {
      ctx.requireAuth();
      const postDB = addPost(post, ctx.userLoged.id);
      return postDB.id;
    }
  },
};
  // Coordinates: new GraphQLScalarType({
  //   name: 'Coordinates',
  //   description: 'Array of coordinates for all dimensions',
  //   parseValue(value) {
  //     return value;
  //   },
  //   serialize(value) {
  //     return value;
  //   },
  //   parseLiteral(ast) {
  //     if (ast.kind === Kind.ARRAY) {
  //       return ast.value;
  //     }
  //     return null;
  //   },
  // }),
  // Date: new GraphQLScalarType({
  //   name: 'Date',
  //   description: 'Date',
  //   parseValue(value) {
  //     return new Date(value); // value from the client
  //   },
  //   serialize(value) {
  //     return value.getTime(); // value sent to the client
  //   },
  //   parseLiteral(ast) {
  //     if (ast.kind === Kind.INT) {
  //       return parseInt(ast.value, 10); // ast value is always in string format
  //     }
  //     return null;
  //   },
  // }),

const server = new GraphQLServer({
  typeDefs,
  resolvers,
  context(request) {
    const token = request.request.headers.token;
    let user = null;
    if (token) {
      try {
        user = jwt.verify(token, 'secret');
      } catch (error) {
        
      }
    }
    return {
      userLoged: user,
      requireAuth() {
        if (!token || !user) {
          throw new Error('Login failed');
        }
        return true;
      }
    }
  }
});

server.start(() => console.log('Server is running on localhost:4000'));