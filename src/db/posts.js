import db from './db';
const shortid = require('shortid');
// const Posts = [
//   {
//     id: 1,
//     titulo: 'Post 2',
//     userId: 2,
//     comentarios: [1, 2],
//   }
// ];

function getPost(id) {
  return db.get('posts').find({ id }).value();
}

function addPost(post, userId) {
  post.userId = userId;
  post.id = shortid.generate();
  db.get('posts')
    .push(post)
    .write();
  return post;
}

export {
  getPost,
  addPost,
};