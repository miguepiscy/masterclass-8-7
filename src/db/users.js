import db from './db';
const shortid = require('shortid');

function getUser(id) {
  const user =  db.get('users').find({ id }).value();
  return new Promise((resolve) => resolve(user));
}

function addUser(user) {
  user.id = shortid.generate();
  db.get('users')
    .push(user)
    .write();
  return user;
}

export {
  getUser,
  addUser,
};